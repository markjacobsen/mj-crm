package net.markjacobsen.crm;

import net.markjacobsen.crm.domain.Contact;

public interface Contacts {
	/**
	 * Get a contact from an email address
	 * @param email
	 * @return 
	 */
	Contact getContactFromEmail(String email) throws CrmException;
}
