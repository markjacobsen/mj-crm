package net.markjacobsen.crm;

public class CrmException extends Exception {
	private static final long serialVersionUID = 1L;

	public CrmException(Throwable exception) {
		super(exception);
	}
	
	public CrmException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public CrmException(String message) {
		super(message);
	}
}
