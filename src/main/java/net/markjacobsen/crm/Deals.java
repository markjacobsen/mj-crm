package net.markjacobsen.crm;

import java.util.List;

import net.markjacobsen.crm.domain.Deal;

public interface Deals {
	/**
	 * Get all deals
	 * @return List of all deals
	 */
	List<Deal> getDeals();
	
	/**
	 * Set the stage/milestone for a given deal
	 * @param deal Deal to set the stage/milestone for
	 * @param stage Stage to set the deal to
	 * @throws CrmException
	 */
	void setDealStage(Deal deal, String stage) throws CrmException;
}
