package net.markjacobsen.crm.domain;

public class Contact {
	String id;
	String firstName;
	String lastName;
	String company;
	String title;
	String mobilePhone;
	String workPhone;
	String homePhone;
	String workEmail;
	String homeEmail;
	String otherEmail;
	
	public String getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getCompany() {
		return company;
	}
	public String getTitle() {
		return title;
	}
	public String getPhoneMobile() {
		return mobilePhone;
	}
	public String getPhoneWork() {
		return workPhone;
	}
	public String getPhoneHome() {
		return homePhone;
	}
	public String getEmailWork() {
		return workEmail;
	}
	public String getEmailHome() {
		return homeEmail;
	}
	public String getEmailOther() {
		return otherEmail;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setPhoneMobile(String phone) {
		this.mobilePhone = phone;
	}
	public void setPhoneWork(String phone) {
		this.workPhone = phone;
	}
	public void setPhoneHome(String phone) {
		this.homePhone = phone;
	}
	public void setEmailWork(String email) {
		this.workEmail = email;
	}
	public void setEmailHome(String email) {
		this.homeEmail = email;
	}
	public void setEmailOther(String email) {
		this.otherEmail = email;
	}
	
	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", company=" + company
				+ ", title=" + title + ", mobilePhone=" + mobilePhone + ", workPhone=" + workPhone + ", homePhone="
				+ homePhone + ", workEmail=" + workEmail + ", homeEmail=" + homeEmail + ", otherEmail=" + otherEmail
				+ "]";
	}
}
