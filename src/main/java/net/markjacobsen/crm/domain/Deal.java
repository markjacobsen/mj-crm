package net.markjacobsen.crm.domain;

public class Deal {
	private String id;
	private String name;
	private String desc;
	
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return desc;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDescription(String description) {
		this.desc = description;
	}
}
