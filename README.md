# mj-crm

Interfaces for generic CRM functions.
Specific implementations should implement these methods and use 
the domain objects provided. That would allow you to swap out 
your CRM provider at any time, but keep code using the CRM the 
same.

